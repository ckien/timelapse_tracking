#!/usr/bin/env python

import subprocess
import shutil
import os.path as osp
import misc

year = 2019 

workspace = '/home/ckienholz/Dropbox/Public/SuiB/camera/{}'.format(year)
workspace2 = '/home/ckienholz/Dropbox/Public/SuiB/camera/{}/movie'.format(year)
workspace3 = '/home/ckienholz/Dropbox/Public/SuiB/camera/{}/movie_high_res'.format(year)

dim1 = 2000
dim2 = -10
   
pic_diff = misc.read_pickle(osp.join(workspace, 'switch.pickle'))

# paths to shell scripts
time_lapse_shell = '/home/ckienholz/Dropbox/Mendenhall/Scripts/timelapse_tracking/timelapse.sh'
gif_shell = '/home/ckienholz/Dropbox/Mendenhall/Scripts/timelapse_tracking/gif.sh'  

# run script only if a new photo has been added    
if pic_diff > 0:
     
    moviename = 'SuiB_{}_timelapse'.format(year)
    
    subprocess.call([time_lapse_shell, workspace2, str(int(dim1)), str(int(dim2)), 
                     moviename, '12', '*{}*0600.png'.format(year)])
    
    subprocess.call([gif_shell, workspace2, '15', '*{}*0600.png'.format(year), moviename])
                     
    #shutil.copy(workspace2 + '/' + moviename + '.mp4', workspace)
    shutil.copy(workspace2 + '/' + moviename + '.avi', osp.dirname(workspace))
    
    shutil.copy(workspace2 + '/' + moviename + '.gif', osp.dirname(workspace))
    
    #--------
                     
    moviename = 'SuiB_{}_timelapse_vector'.format(year)  
    
    subprocess.call([time_lapse_shell, workspace, str(int(dim1)), str(int(dim2)), 
                     moviename, '7', '*Processed_st*{}*.png'.format(year)])
    
    subprocess.call([gif_shell, workspace, '20', '*{}*.png'.format(year), moviename])
    
    # copy one level up (to maintain the dropbox link)                 
    shutil.copy(workspace + '/' + moviename + '.avi', osp.dirname(workspace))
    
    shutil.copy(workspace + '/' + moviename + '.gif', osp.dirname(workspace))
                     
    #--------
                     
#    moviename = 'SuiB_2019_timelapse_high_res'   
#    
#    subprocess.call([time_lapse_shell, workspace3, str(int(dim1)), str(int(dim2)), 
#                     moviename, '10', '*2019*.png'])
#                     
#    #shutil.copy(workspace3 + '/' + moviename + '.mp4', workspace)
#    shutil.copy(workspace3 + '/' + moviename + '.avi', workspace)
                     
else:
    pass