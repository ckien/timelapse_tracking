# -*- coding: utf-8 -*-
"""
Created on Wed Jun 06 2018

@author: Ch.Kienholz
"""

import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt

import os.path as osp
from PIL import Image

import numpy as np
import matplotlib.dates as mdates

import misc
colors_ck = misc.colors_ck()

mpl.style.use('classic')

# set Arial as standard font
mpl.rc("font", **{"sans-serif": ["Arial"]})

   
#%%

def load_2019_SuiB_level():
    '''
    Function to compile 2019 water level measurements in a dataframe
    '''
   
    time = ['2019-05-28 06:00', '2019-06-02 06:00', '2019-06-07 06:00', 
            '2019-06-15 06:00', '2019-06-16 06:00', '2019-06-17 06:00', 
            '2019-06-18 06:00', '2019-06-19 06:00', '2019-06-20 06:00',
            '2019-06-21 06:00', '2019-06-22 06:00', '2019-06-23 06:00', 
            '2019-06-24 06:00', '2019-06-25 06:00', '2019-06-26 06:00', 
            '2019-06-27 06:00', '2019-06-28 06:00', '2019-06-29 06:00',
            '2019-06-30 06:00', '2019-07-01 06:00', '2019-07-02 06:00',
            '2019-07-03 06:00', '2019-07-04 09:00', '2019-07-05 06:00', 
            '2019-07-06 06:00', '2019-07-07 06:00', '2019-07-07 12:00', 
            '2019-07-07 18:00', '2019-07-08 06:00', '2019-07-09 06:00', 
            '2019-07-10 06:00', '2019-07-11 06:00', '2019-07-12 06:00', 
            '2019-07-13 06:00', '2019-07-14 06:00', '2019-07-14 15:00', 
            '2019-07-15 09:00', '2019-07-16 06:00']
            
    time = [pd.to_datetime(t) for t in time]
    
    elevations = [400, 404.5, 407, 413.5, 414.5, 415.8, 417.7, 419.0, 419.8, 
                  420.5, 421.4, 422.3, 423.1, 423.9, 424.8, 426.0, 427.4, 428.6,
                  429.8, 430.7, 431.9, 433.1, 434.1, 435.2, 436.4, 437.6, 437.9,
                  437.9, 437.7, 437.15, 436.65, 436.36, 435.84, 434.63, 428, 418, 
                  409, 403]
    
    series_19 = pd.Series(index=time, data=elevations)
    
    return series_19
     
def plot_suiB_website(automatic=0):

    fig_workspace = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera'
    fig_workspace2 = r'C:\Users\Ch.Kienholz\Google Drive\SuiB'
    logo_workspace = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\logos'
    data_workspace = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\data'      
    
    plt.close('all')
        
    plt.ioff()
        
    fig = plt.figure(figsize=(8.625, 7), facecolor='w')
    ax1 = fig.add_axes([0.08, 0.04, 0.905, 0.91], anchor='SW', zorder=10)
    
    plot_early_years = 0 # switch to plot years before 2018
    
    if plot_early_years == 1:
               
        df_2012 = pd.read_pickle(osp.join(data_workspace, 'SuiB_water_level_2012.pickle'))
        df_2014 = pd.read_pickle(osp.join(data_workspace, 'SuiB_water_level_2014.pickle'))
        df_2016 = pd.read_pickle(osp.join(data_workspace, 'SuiB_water_level_2016.pickle'))
    
        df_2012_shifted = df_2012.copy()
        df_2012_shifted.index = df_2012_shifted.index.map(lambda t: t.replace(year=2018))
        df_2012_shifted = df_2012_shifted.dropna()
        ax1.plot_date(df_2012_shifted.index, df_2012_shifted, '-', lw=2.5, label='2012',
                      color=colors_ck.brown, zorder=-100)
                      
        print 'max 2012: {}'.format(df_2012.max())
           
        df_2014_shifted = df_2014.copy()
        df_2014_shifted.index = df_2014_shifted.index.map(lambda t: t.replace(year=2018))
        df_2014_shifted = df_2014_shifted.dropna()
        ax1.plot_date(df_2014_shifted.index, df_2014_shifted, '-', lw=2.5, label='2014', 
                      color=colors_ck.orange, zorder=-100)
                      
        print 'max 2014: {}'.format(df_2014.max())
        
        df_2016_shifted = df_2016.copy()
        df_2016_shifted.index = df_2016_shifted.index.map(lambda t: t.replace(year=2018))
        df_2016_shifted = df_2016_shifted.dropna()
        ax1.plot_date(df_2016_shifted.index, df_2016_shifted, '-', lw=2.5, label='2016', 
                      color=colors_ck.green, zorder=-100)
                      
        print 'max 2016: {}'.format(df_2016.max())
    
    # load 2018 gauge data            
    df_2018 = pd.read_pickle(osp.join(data_workspace, 'SuiB_water_level_2018.pickle'))
    
    ax1.plot_date(df_2018.index, df_2018, '-', color=colors_ck.blue, label='2018', 
                  lw=2.5, zorder=10)  

    df_2019 = load_2019_SuiB_level()
    
    if automatic == 1:
        
        water_level_m = pd.read_pickle(osp.join(data_workspace, 'pixel_tracking_metric.pickle'))
                
        water_level_m_diff = water_level_m.diff()        
        
        water_level_m = water_level_m['2019-07-16 05:00':]
        water_level_m_diff = water_level_m_diff['2019-07-16 05:00':]

        water_level_m_diff = water_level_m_diff[water_level_m > 420]
        water_level_m_diff = water_level_m_diff[water_level_m < 439]
        
        water_level_m = water_level_m[water_level_m > 420]
        water_level_m = water_level_m[water_level_m < 439]
        
        # max water level increase allowed
        thres = 2.4
        
        if water_level_m_diff.max() > thres:
            
            cutoff_index = water_level_m[water_level_m_diff < thres].index[-1]
            
            water_level_m = water_level_m[:cutoff_index]
                   
        df_2019 = pd.concat([df_2019, water_level_m], axis=0)
        
    df_2019.index = df_2019.index.map(lambda t: t.replace(year=2018))
    
    ax1.plot_date(df_2019.index, df_2019, '-', lw=2.5, label='2019', 
                  color=colors_ck.red, zorder=100)  
                  
    text = '{0}: {1:.1f} m'.format(dt.datetime.strftime(df_2019.index[-1], 
                                    '2019-%m-%d'), df_2019[-1])
    
    ax1.annotate(text, xy=(mdates.date2num(df_2019.index[-1]), df_2019[-1]), 
            xycoords='data', xytext=(-120, -50), textcoords='offset points', 
            arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=0'), 
            fontsize=12, zorder=1000) 
        
    #-------------------------------
    
    misc.format_date_axis(plt, ax1, 'day_month_year', rot=70)
           
    ax1.set_ylabel('Water level elevation (m a.s.l.)', size=13)
              
    ticks = np.arange(380, 462, 10.0)
    ax1.set_yticks(ticks)
    
    misc.minorlabeling(ax1, np.arange(380, 462, 1), 'y')
        
    mindate = pd.to_datetime('2018-05-19 00:00')
    maxdate = pd.to_datetime('2018-07-26 00:00')

    ax1.set_xlim(mindate, maxdate)        
    ax1.set_ylim(392, 461) 
    
    [handles, labels] = ax1.get_legend_handles_labels()
       
    # puts the upper right corner of the legend at 99% (x-extent) and 98% (y-extent)
    ax1.legend(handles, labels, loc='upper right', bbox_to_anchor=(0.99, 0.98), 
               frameon=0, fontsize=13, numpoints=1, ncol=1)
                    
    # annotate ice dam height                
    ax1.plot_date([pd.to_datetime('2018-06-09 12:00'), pd.to_datetime('2018-07-30 18:00')], 
                   [437.5, 437.5], ':', color='darkgray', lw=1.5, zorder=-100)
                                
    ax1.annotate('Ice dam height 2019 (pre-overflow)', xy=(mdates.date2num(pd.to_datetime('2018-06-11 15:00')), 438), 
                xycoords='data', xytext=(-55.0, 30), textcoords='offset points', 
                arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=0'), 
                fontsize=12, zorder=1000)
                                
    #------------------------------- 
                                
    axlocations = [[0.125, 0.79, 0.125, 0.125], 
                   [0.105, 0.69, 0.14, 0.14],
                   [0.108, 0.59, 0.13, 0.13],
                   [0.123, 0.44, 0.115, 0.115]]
                   
    logos = ['UAS_logo.png', 'AKCASC_color.png', 'USGS.png', 'nws_logo2.png']
    
    zorders = [10, 10, 11, 11]
    
    for axlocation, logo, zorder in zip(axlocations, logos, zorders):
        
        axins = fig.add_axes(axlocation, anchor='SW', zorder=zorder)
            
        axins.patch.set_visible(False)
        
        image = osp.join(logo_workspace, logo)
                                  
        frame = np.array(Image.open(image))
        axins.imshow(frame)            
                        
        axins.set_xlim([0, frame.shape[1]])
        axins.set_ylim([frame.shape[0], 0])
        axins.axis('off') 
        
    #--------------------------------
                   
    misc.format_date_axis(plt, ax1, 'weeks', rot=0)
    
    weeks = mpl.dates.WeekdayLocator(byweekday=mpl.dates.MO, interval=1)
    days = mpl.dates.DayLocator()
    ax1.xaxis.set_major_locator(weeks)
    ax1.xaxis.set_minor_locator(days)

    Fmt = mpl.dates.DateFormatter('%b %d')
    ax1.xaxis.set_major_formatter(Fmt)
    
    fig.savefig(osp.join(fig_workspace, 'Plot_water_level.png'), dpi=250)
    fig.savefig(osp.join(fig_workspace2, 'Plot_water_level.png'), dpi=250)
    
if __name__ == '__main__':
    
    automatic = 0
    
    plot_suiB_website(automatic)