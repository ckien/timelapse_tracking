# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 2019

@author: Ch.Kienholz
"""

import pandas as pd
import datetime as dt

from collections import OrderedDict
import pickle

#%%

def write_pickle(filename, variable):
    with open(filename, 'wb') as p:
        pickle.dump(variable, p)
    del p

def read_pickle(filename):
    with open(filename, 'rb') as p:
        output = pickle.load(p)
    del p
    return output

def clean_up_legend(ax):
    handles, labels = ax.get_legend_handles_labels()
    # remove duplicate lables and handles
    by_label = OrderedDict(zip(labels, handles))
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(by_label.keys(), by_label.values()), key=lambda t: t[0]))
    
    return [handles, labels]    
    
def format_date_axis(plt, ax, kw, rot=45, yr=10, sbyr=1):
    from matplotlib.dates import YearLocator
    from matplotlib.dates import MonthLocator
    from matplotlib.dates import DayLocator
    from matplotlib.dates import WeekdayLocator
    from matplotlib.dates import DateFormatter
    from matplotlib.dates import MO
        
    if kw == 'tenyear_year':
        years10 = YearLocator(yr)
        years = YearLocator(sbyr)
        Fmt = DateFormatter('%Y')
    
        # format the ticks
        ax.xaxis.set_major_locator(years10)
        ax.xaxis.set_major_formatter(Fmt)
        ax.xaxis.set_minor_locator(years)
        
    if  kw == 'months':
        years = YearLocator(1)
        months = MonthLocator()
        Fmt = DateFormatter('%m/%y')
        ax.xaxis.set_major_locator(months)
        ax.xaxis.set_major_formatter(Fmt)
        
    if  kw == 'weeks':
        weeks = WeekdayLocator(byweekday = MO, interval=1)
        days = DayLocator() # every month
        Fmt = DateFormatter('%m/%d')
        ax.xaxis.set_major_locator(weeks)
        ax.xaxis.set_major_formatter(Fmt)
        ax.xaxis.set_minor_locator(days)
        
    if  kw == 'days':
        years = YearLocator(1)
        months = MonthLocator()
        days = DayLocator()
        Fmt = DateFormatter('%m/%d')
        ax.xaxis.set_major_locator(days)
        ax.xaxis.set_major_formatter(Fmt)
        
    if  kw == 'day_month_year':
        years = YearLocator(1)
        months = MonthLocator()
        days = DayLocator()
        Fmt = DateFormatter('%m/%d/%y')
        ax.xaxis.set_major_locator(days)
        ax.xaxis.set_major_formatter(Fmt)
        
    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=rot)  

def minorlabeling(ax, ticks, side):
    if side == 'x':
        ax.set_xticks((ticks), minor = 1)
        ax.tick_params('x', length=2, width=.5, which='minor')
    elif side == 'y':
        ax.set_yticks((ticks), minor = 1)
        ax.tick_params('y', length=2, width=.5, which='minor') 
        
def annotatefun(ax, textlist, xstart, ystart, ydiff=0.05, fonts=12, col='k', ha='left'):
    counter = 0
    for textstr in textlist:
        ax.text(xstart, (ystart - counter * ydiff), textstr, fontsize=fonts, 
                ha=ha, va='center', transform=ax.transAxes, color=col)
        counter += 1
        
def digit_formatter(inputnumber, digits):  
    return ("{0:0.%sf}"%(digits)).format(round(inputnumber, digits) + 0.0)
	
def hour_rounder(t):
    '''
    rounds to nearest hour by adding a timedelta hour if minute >= 30
    '''
    return (t.replace(second=0, microsecond=0, minute=0, hour=t.hour) 
    + dt.timedelta(hours=t.minute / 30))
    
def symbols_ck(keyword, hspace=-0.25):
    
    if keyword == 'degreeC':
        symb = '$\hspace{-0.1}{^\circ}\hspace{-0.25}$C'
    elif keyword == 'degree':
        symb = '$\hspace{-0.1}{^\circ}\hspace{-0.25}$'
    elif keyword == 'r2':
        symb = 'r$\mathregular{^2} \!$'
    elif keyword == 'km3':
        symb = 'km$\mathregular{^3} \!$'
    elif keyword == 'km2':
        symb = 'km$\mathregular{^2} \!$'
    elif keyword == 'm2':
        symb = 'm$\mathregular{^2} \!$'
    elif keyword == 'm3':
        symb = 'm$\mathregular{^3} \!$'
    elif keyword == 'delta':
        symb = '$\Delta\!\!$ '
    elif keyword == 'mps':
        symb = unicode('m s$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mpd':
        symb = unicode('m d$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mpy':
        symb = unicode('m a$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mwepy':
        symb = unicode('m w.e. a$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    else:
        raise ValueError('WRONG KEYWORD')
        
    return symb  
	
class colors_ck():
    '''
    Colors implemented as class
    '''
    
    def __init__(self):

        colornamelist = ['orange', 'orange_light', 'green', 'green_light',
        'purple_light', 'purple', 'blue', 'blue_light', 'red_light', 'red',
        'brown', 'brown_light', 'darkgray']    
        
        colorlist = [[255,127,0], [253,191,111], [51,160,44], [178,223,138],
                     [202,178,214], [106,36,194], [25,116,210], [166,206,227],
                     [252,116,94],[227,26,28], [106,53,24],[177,89,40],[50,50,50]]  
    
        numbers = range(0, len(colornamelist), 1)
        colordict = {}
    
        for n in numbers:
            colorlist[n][0] = colorlist[n][0]/255.0
            colorlist[n][1] = colorlist[n][1]/255.0
            colorlist[n][2] = colorlist[n][2]/255.0
    
        for n, colorname in zip(numbers, colornamelist):
            colordict[colorname] = colorlist[n]
        
	  # converts the dictionary to class attributes
        self.__dict__.update(colordict)
        
#%%
        
def pixel_elev_from_meters(year, meter_elev):
    '''
    converts meter elevations into pixel elevations to plot a scale bar on 
    the oblique image
    pixel_elev 368 (in 2019) = 477.64 m a.s.l.
    pixel_elev 483.5 (in 2019) = 417.8 m a.s.l.
    1.930147 pixel vertical thus correspond to 1 m vertical displacement
    
    meter elevations were sampled in Metashape on drone-derived DEM while 
    the corresponding pixel elevations were sampled on oblique photo (in ArcGIS)
    note this is only an approximation
    '''
    
    if year == 2019:
        pixel_elev = 368
        x_pixel = 191
    if year == 2018:
        pixel_elev = 427.25
        x_pixel = 286
    if year == 2017:
        pixel_elev = 378
        x_pixel = 270
    if year == 2016:
        pixel_elev = 388.15
        x_pixel = 108
           
    m_diff = meter_elev - 477.64
    
    y_pixel = pixel_elev - m_diff * 1.930147 
    
    return [x_pixel, y_pixel] 