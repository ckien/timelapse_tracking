#!/bin/bash 

# -x

# This creates a time-lapse video that works in media player

workspace=$1
xscale=$2
yscale=$3
videoname=$4
framenumber=$5
searchpattern=$6

cd $workspace

mencoder mf://$searchpattern -mf fps=$framenumber -o $videoname.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=20000000 -vf scale=$xscale:$yscale

# avi to mp4
ffmpeg -i $videoname.avi -c:v libx264 -crf 19 -preset slow -c:a libfdk_aac -b:a 192k -ac 2 $videoname.mp4
