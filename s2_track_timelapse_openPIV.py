#!/usr/bin/env python
"""
Created on Tue Jun 05 2018

@authors: Ch.Kienholz, J.Amundson
"""

import numpy as np
from PIL import Image
import openpiv.process
import openpiv.tools
import openpiv.validation
import time
import glob, os
from shutil import copyfile, move
import os.path as osp
import misc

def tracking_func(args):

    file_a, file_b, counter = args
    
    ws = file_a[0:file_a.rfind('/')]
    print ws
    
    # Specifiy window size, search area, and overlap for the image cross-correlation. 
    # Because openPIV uses FFT, make these a power of 2.
    n = 6
    window_sz = 2 ** n
    ovrlap = 2 ** (n-1)
    search_area_sz = 2 ** n   
      
    # load files
    im_a = np.array(Image.open(file_a).convert('L')).astype(np.int32)
    im_b = np.array(Image.open(file_b).convert('L')).astype(np.int32)
    
    filename = file_a # filename needed for saving purposes
    
    # run openPIV
    u, v, sig2noise = openpiv.process.extended_search_area_piv(im_a, im_b,window_size=window_sz, 
                                                               overlap=ovrlap, dt=2.0, 
                                                               search_area_size=search_area_sz, 
                                                               sig2noise_method='peak2peak')
    
    x, y = openpiv.process.get_coordinates(image_size=im_a.shape, window_size=window_sz, overlap=ovrlap)
    
    # filter results
    #	u, v = openpiv.filters.gaussian( u, v, 3)
    # u, v = openpiv.filters.replace_outliers( u, v, method='localmean', max_iter=10, kernel_size=2 )
    
    # remove outliers
    if osp.basename(os.path.dirname(file_a)) == 'pixel_tracking_plot':
        u, v, mask = openpiv.validation.sig2noise_val(u, v, sig2noise, threshold=0.8)    
    else:
        u, v, mask = openpiv.validation.sig2noise_val(u, v, sig2noise, threshold=1.2)
    
    np.savez(filename + '.npz', x=x, y=y, u=u, v=v)

def run_tracking(workspace, pic_diff, process_all=0):
        
    index = -1 * pic_diff - 1
    
    if process_all == 0: 
    
        for files in sorted(glob.glob(workspace + '/Suicide*.jpg'))[index:]:
            os.rename(files, files[:-4] + '-1.jpg')
            
    else:
        for files in sorted(glob.glob(workspace + '/Suicide*.jpg')):
            os.rename(files, files[:-4] + '-1.jpg')
        
    # List of files to analyze
    files = sorted(glob.glob(workspace + '/*-1.jpg'))
    
    # Use a for loop to process all of the images in a directory
    for j in xrange(0, len(files)-1):
        temp1 = files[j]
        temp2 = files[j+1]
        copyfile(temp2, temp1[0:-6] + '-2.jpg')
    
    move(files[j+1], files[j+1][0:-6] + '.jpg')
    
    print 'starting:'
    start = time.time()
    
    task = openpiv.tools.Multiprocesser(data_dir=workspace, pattern_a='*-1.jpg', 
                                        pattern_b='*-2.jpg')
    task.run(func=tracking_func, n_cpus=1)
    
    print 'time elapsed: %f seconds'%(time.time() - start)
    
    # Clean up files after processing
    for files in glob.glob(workspace + '/*-2.jpg'):
        os.remove(files)
    
    for files in glob.glob(workspace + '/*-1.jpg'):
        os.rename(files, files[:-6] + '.jpg')
    
    for files in glob.glob(workspace + '/*jpg.npz'):
        os.rename(files, files[:-10] + '.npz')
        
def main():

    basedir = '/home/ckienholz/Dropbox/Public/SuiB/camera' 
    year = 2019
    directory = '{}/{}'.format(basedir, year)
    
    if year == 2019:
        directory2 = '{}/{}/pixel_tracking_plot'.format(basedir, year)
    
    pic_diff = misc.read_pickle(osp.join(directory, 'switch.pickle'))
    
    if pic_diff > 0:
        run_tracking(directory, pic_diff, process_all=0) 
        
        if year == 2019:
            run_tracking(directory2, pic_diff, process_all=0)
    else:
        pass    
       
if __name__ == '__main__':
    
    main()