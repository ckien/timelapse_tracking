# -*- coding: utf-8 -*-
"""
Created on Tue Jun 05 2018

@author: Ch.Kienholz
"""

import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import glob
import datetime as dt
import matplotlib as mpl
import os.path as osp
import pandas as pd
import misc

colors_ck = misc.colors_ck()

# set Arial as standard font
mpl.rc("font", **{"sans-serif": ["Arial"]}) # "size": fontsize}
   
#%%
   
def plot_latest_images(workspace, start_index=0):
 
    scalingfactor = 10
    
    # List of files to analyze
    files = sorted(glob.glob(workspace + '/*.npz'))
    images = sorted(glob.glob(workspace + '/*.jpg'))
    
    if start_index != 0:
        
        # for example cases where not the last 4 images are plotted
        files = files[(start_index - 4):(start_index)]
        images = images[(start_index - 5):(start_index)]
        
    else:
        
        files = files[-4:] # just take the latest 4
        images = images[-5:]
    
    plt.ioff()
        
    thr = 80 # to filter
    thr2 = 10 # to plot
    
    fig = plt.figure(figsize=(16, 11.5), facecolor='w')
    
    for c, f in enumerate(files):
    	
        data = np.load(f)

        date1 = dt.datetime.strptime(osp.basename(images[c]), 
                                     'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        date2 = dt.datetime.strptime(osp.basename(images[c + 1]), 
                                     'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        
        date1_str = dt.datetime.strftime(date1, '%Y-%m-%d %I:%M %p')
        date2_str = dt.datetime.strftime(date2, '%Y-%m-%d %I:%M %p')
        
        timediff = round((date2 - date1).total_seconds() / 3600.0, 1)
        
        scaling = 24.0 / timediff
        
        x = data['x']
        y = data['y']
        u = data['u'] * scaling
        v = data['v'] * scaling
        
        speed = np.sqrt((u ** 2) + (v ** 2)) 
        
        x[speed > thr] = np.nan
        y[speed > thr] = np.nan
        u[speed > thr] = np.nan
        v[speed > thr] = np.nan
         
        im_b = np.array(Image.open(images[c + 1]).convert('L'))
               
        ax = fig.add_subplot(2, 2, c + 1)  
        ax.set_axis_off()
        
        # plot second image        
        ax.imshow(im_b, cmap='gray', alpha=0.8)
        quivers = ax.quiver(x, im_b.shape[0] - y, u * scalingfactor, v * scalingfactor, 
                            speed, clim=[0.0, thr2], cmap='rainbow', units='x', 
                            scale=1.0, width=3.0)
        
        #--------------------------------------------
        
        for meter in range(380, 481, 10):
            
            [x_pixel, y_pixel] = misc.pixel_elev_from_meters(date1.year, meter)
            
            ax.plot([x_pixel - 2, x_pixel + 2], [y_pixel, y_pixel], '-', color='red', lw=1.2)
            
            ax.text(x_pixel + 19, y_pixel, str(meter) + ' m', color='white', fontsize=8, 
                    verticalalignment='center', zorder=100)
        
        misc.annotatefun(ax, ['Source: USGS'], 0.018, 0.03, 0.03, 13, col='white')
        misc.annotatefun(ax, ['Vector magn. scaled {}x'.format(scalingfactor)], 
                             0.71, 0.03, 0.03, 13, col='white')
        
        plt.title('{} - {} ({} hours)'.format(date1_str, date2_str, timediff), 
                  size=14, loc='center')
         
    fig.tight_layout(w_pad=-1.0, h_pad=-1.5) 
            
    fig.subplots_adjust(right=0.93, left=0.015, top=0.98, bottom=0.003)
    
    cax = fig.add_axes([0.9425, 0.25, 0.015, 0.5])
    cb = fig.colorbar(quivers, cax=cax, ticks=np.arange(0, thr2 + 0.1, 1.0)) # ticks = np.arange(0, 1.05, 0.1)
    cb.set_label(label='Pixels/day', labelpad=10, size=14) 

    fig.savefig(osp.dirname(workspace) + '/Processed_latest_images.png', dpi=200)	
        
    plt.close(fig)
    
    
def plot_each_image(workspace, logo_ws, pic_diff, plot_all=0):
    
    scalingfactor = 10
    
    # List of files to analyze
    files = sorted(glob.glob(workspace + '/*.npz'))
    images = sorted(glob.glob(workspace + '/*.jpg'))
    
    if plot_all == 1:
        
        files = files
        images = images
        
    else:
        
        index = -1 * pic_diff
        index2 = index - 1         
        
        files = files[index:]
        images = images[index2:]
        
    plt.ioff()
        
    thr = 80
    thr_col = 10
    
    for c, f in enumerate(files):
        
        fig = plt.figure(figsize=(11.5, 8), facecolor='w')
    	
        data = np.load(f)
                
        date1 = dt.datetime.strptime(osp.basename(images[c]), 
                                     'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        date2 = dt.datetime.strptime(osp.basename(images[c + 1]), 
                                     'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        
        date1_str = dt.datetime.strftime(date1, '%Y-%m-%d %I:%M %p')
        date2_str = dt.datetime.strftime(date2, '%Y-%m-%d %I:%M %p')
        
        timediff = round((date2 - date1).total_seconds() / 3600.0, 1)
        
        scaling = 24.0 / timediff
        
        x = data['x']
        y = data['y']
        u = data['u'] * scaling
        v = data['v'] * scaling
        
        speed = np.sqrt((u ** 2) + (v ** 2)) 
        
        x[speed > thr] = np.nan
        y[speed > thr] = np.nan
        u[speed > thr] = np.nan
        v[speed > thr] = np.nan
        
        # plot the second image
        im_b = np.array(Image.open(images[c + 1]).convert('L'))
               
        ax = fig.add_subplot(1, 1, 1)  
        ax.set_axis_off()
                
        ax.imshow(im_b, cmap='gray', alpha=0.8)
        quivers = ax.quiver(x, im_b.shape[0] - y, u * scalingfactor, v * scalingfactor, 
                            speed, clim=[0.0, thr_col], cmap='rainbow', units='x', scale=1.0, 
                            width=2)
                                                        
        #--------------------------------------------
        # agency logos  
        
        axlocations = [[0.785, 0.12, 0.12, 0.12], 
                       [0.75, 0.02, 0.14, 0.14]]
                       
        logos = ['UAS_logo_white.png', 'AKCASC_white.png']
                
        for axlocation, logo in zip(axlocations, logos):
            
            axins = fig.add_axes(axlocation, anchor='SW', zorder=10)
                
            axins.patch.set_visible(False)
            
            image = osp.join(logo_ws, logo)
                                      
            frame = np.array(Image.open(image))
            axins.imshow(frame)            
                            
            axins.set_xlim([0, frame.shape[1]])
            axins.set_ylim([frame.shape[0], 0])
            axins.axis('off')                            
                                                       
        #-------------------------------------------- 
        # vertical scale
                                                       
        for meter in range(380, 481, 10):
            
            [x_pixel, y_pixel] = misc.pixel_elev_from_meters(date1.year, meter)
            
            ax.plot([x_pixel - 2, x_pixel + 2], [y_pixel, y_pixel], '-', color='red', lw=1.2)
            
            ax.text(x_pixel + 19, y_pixel, str(meter) + ' m', color='white', fontsize=8, 
                    verticalalignment='center', zorder=100)
                
        misc.annotatefun(ax, ['Image: USGS'], 0.018, 0.02, 0.03, 13, col='white')
        misc.annotatefun(ax, ['Vector magnitude scaled {}x'.format(scalingfactor)], 
                        0.39, 0.02, 0.03, 11, col='white')
        
        ax.title.set_text('{} - {} ({} hours)'.format(date1_str, date2_str, timediff))
        ax.title.set_size(14)
        ax.title.set_horizontalalignment('center')
         
        fig.tight_layout(w_pad=-1.0, h_pad=-2.0) 
            
        fig.subplots_adjust(right=0.93, left=0.002, top=0.95, bottom=0.005)
    
        cax = fig.add_axes([0.93, 0.2, 0.015, 0.6])
        cb = fig.colorbar(quivers, cax=cax, ticks=np.arange(0, thr_col + 0.1, 1.0))
        cb.set_label(label='Pixels/day', labelpad=7, size=14) 
        
        date1_str_file = dt.datetime.strftime(date1, '%Y%m%d_%H%M')

        fig.savefig(workspace + '/Processed_st_{}.png'.format(date1_str_file), dpi=100)
        
        # hack to have the last image shown at the beginning in the .gif
        fig.savefig(workspace + '/Processed_st_20190101_0000.png', dpi=100)
        
        if date1.year >= 2019 and osp.basename(workspace) != 'pixel_tracking_plot':
            fig.savefig(osp.dirname(workspace) + '/Processed_latest_image.png', dpi=200)
        
        plt.close(fig)
        
def pixel_time_series_2019():
    '''
    samples the displacement at selected pixel locations and plots them 
    as a time series
    '''
    year = 2019
    workspace = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\{}\pixel_tracking_plot'.format(year)    
        
    dir_logos = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\logos'
    dir_data = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\data'
    dir_fig = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera'
    
    fig = plt.figure(figsize=(12, 6.0), facecolor='w')
    ax1 = fig.add_axes([0.06, 0.04, 0.93, 0.91], anchor='SW', zorder=10)
    ax2 = fig.add_axes([0.08, 0.44, 0.45, 0.45], anchor='SW', zorder=100)
        
    timelist = []

    u_list0 = []
    v_list0 = []    
    
    u_list1 = []
    v_list1 = []
    
    u_list2 = []
    v_list2 = []

    u_list3 = []
    v_list3 = []

    u_list4 = []
    v_list4 = []     
            
    [row0, col0] = [22, 2]
    [row1, col1] = [16, 11]
    [row2, col2] = [18, 22]
    [row3, col3] = [15, 30]
    [row4, col4] = [26, 5]
    
    # List of files to analyze
    files = sorted(glob.glob(workspace + '/*.npz'))
    images = sorted(glob.glob(workspace + '/*.jpg'))
       
    for c, f in enumerate(files):
            	
        data = np.load(f)
        
        date1 = osp.basename(images[c]).split('.')[0].split('_')[1]
        date2 = osp.basename(images[c + 1]).split('.')[0].split('_')[1]
        
        date1 = dt.datetime.strptime(date1, '%Y%m%d%H%M%S') - dt.timedelta(hours=8)
        date2 = dt.datetime.strptime(date2, '%Y%m%d%H%M%S') - dt.timedelta(hours=8)
        
        date1 = date1.replace(minute=0, second=0)
        date2 = date2.replace(minute=0, second=0)
        
        timelist.append(date2)
                    
        u_list0.append(data['u'][row0, col0])
        v_list0.append(data['v'][row0, col0])
                    
        u_list1.append(data['u'][row1, col1])
        v_list1.append(data['v'][row1, col1])
        
        u_list2.append(data['u'][row2, col2])
        v_list2.append(data['v'][row2, col2])
        
        u_list3.append(data['u'][row3, col3])
        v_list3.append(data['v'][row3, col3])
        
        u_list4.append(data['u'][row4, col4])
        v_list4.append(data['v'][row4, col4])
        
        if c == len(files) - 1:
            
            x = data['x']
            y = data['y']
         
            im_a = np.array(Image.open(images[c + 1]).convert('L'))
            
            ax2.set_axis_off()
                
            ax2.imshow(im_a, cmap='gray', alpha=1.0)
            
            misc.annotatefun(ax2, ['Image: USGS'], 0.75, 0.04, 0.03, 10, col='white')
    
            ax2.title.set_text('{}'.format(dt.datetime.strftime(date2, '%Y-%m-%d %I:%M %p')))  
            
            # ax2.plot(x[row0, col0], im_a.shape[0] - y[row0, col0], 'o', color=colors_ck.orange, ms=10)
            ax2.plot(x[row1, col1], im_a.shape[0] - y[row1, col1], 'o', color=colors_ck.blue, ms=10)
            # ax2.plot(x[row2, col2], im_a.shape[0] - y[row2, col2], 'o', color=colors_ck.blue, ms=10)
            # ax2.plot(x[row3, col3], im_a.shape[0] - y[row3, col3], 'o', color=colors_ck.blue, ms=10)
            # ax2.plot(x[row4, col4], im_a.shape[0] - y[row4, col4], 'o', color=colors_ck.brown, ms=10)

    #--------------------------------------------------------------------------
    
    df = pd.DataFrame({'time': timelist, 'u0': u_list0, 'v0': v_list0, 
                       'u1': u_list1, 'v1': v_list1, 'u2': u_list2, 'v2': v_list2, 
                       'u3': u_list3, 'v3': v_list3, 'u4': u_list4, 'v4': v_list4})
    df.set_index('time', drop=1, inplace=1)
    
    df['2019-05-18':'2019-05-18'] = 0 
        
    df['v0_cumsum'] = df['v0'].cumsum() - df['v0'].iloc[0] 
    df['v1_cumsum'] = df['v1'].cumsum() - df['v1'].iloc[0]
    df['v2_cumsum'] = df['v2'].cumsum() - df['v2'].iloc[0]
    df['v3_cumsum'] = df['v3'].cumsum() - df['v3'].iloc[0]
    df['v4_cumsum'] = df['v4'].cumsum() - df['v4'].iloc[0]
    
    # ax1.plot_date(df.index, df['v0_cumsum'], '-', color=colors_ck.orange, lw=1.5)
    ax1.plot_date(df.index, df['v1_cumsum'], '-', color=colors_ck.blue, lw=2.0)
    # ax1.plot_date(df.index, df['v2_cumsum'], '-', color=colors_ck.blue, lw=1.7, label='Feature tracking')
    # ax1.plot_date(df.index, df['v3_cumsum'], '-', color=colors_ck.blue, lw=1.5)
        
    df['v1_cumsum'].to_pickle(osp.join(dir_data, 'pixel_tracking.pickle'))
    
    # equation to convert pixel elevations to meters 
    # water_level_m = (df['v1_cumsum'] - 53.2075) * 0.4465 + 426
    # water_level_m = (df['v1_cumsum'] - 53.2075) * 0.325 + 426
    water_level_m = (df['v1_cumsum'] - 89.569559) * 0.27 + 437.7
    
    water_level_m = water_level_m.round(2)
    
    water_level_m.to_pickle(osp.join(dir_data, 'pixel_tracking_metric.pickle'))  

    #--------------------------------------------------------------------------
    # agency logos  
    
    axlocations = [[0.885, 0.18, 0.15, 0.15], 
                   [0.85, 0.05, 0.13, 0.13]]
                   
    logos = ['UAS_logo.png', 'AKCASC_color.png']
            
    for axlocation, logo in zip(axlocations, logos):
        
        axins = fig.add_axes(axlocation, anchor='SW', zorder=10)
            
        axins.patch.set_visible(False)
        
        image = osp.join(dir_logos, logo)
                                  
        frame = np.array(Image.open(image))
        axins.imshow(frame)            
                        
        axins.set_xlim([0, frame.shape[1]])
        axins.set_ylim([frame.shape[0], 0])
        axins.axis('off')  
        
    #--------------------------------------------------------------------------
       
    ax1.set_ylabel('Relative water level (pixels)', size=13)
    
    weeks = mpl.dates.WeekdayLocator(byweekday=mpl.dates.MO, interval=1)
    days = mpl.dates.DayLocator() # every month
    ax1.xaxis.set_major_locator(weeks)
    ax1.xaxis.set_minor_locator(days) 
    
    Fmt = mpl.dates.DateFormatter('%b %d')
    ax1.xaxis.set_major_formatter(Fmt)
    
    ax1.set_xlim(['{}-05-16'.format(year), '{}-8-15'.format(year)])
    ax1.set_ylim([-20, 200])
   
    fig.savefig(osp.join(dir_fig, 'Pixel_tracking_plot.png'), dpi=250) 
    plt.close(fig)
 
        
#%%

def main():

    year = 2019
    dir_C = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\{}'.format(year)
    dir_C2 = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\{}\pixel_tracking_plot'.format(year) 
    dir_logos = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\logos' 
    
    pic_diff = misc.read_pickle(osp.join(dir_C, 'switch.pickle'))
    
    if pic_diff > 0:
    
        plot_latest_images(dir_C)
        
        if year == 2019:
            plot_each_image(dir_C2, dir_logos, pic_diff, plot_all=0)
            pixel_time_series_2019()
            
        plot_each_image(dir_C, dir_logos, pic_diff, plot_all=0)

    else:
        pass  
        
if __name__ == '__main__':
    
    main()