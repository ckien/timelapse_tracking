#!/bin/bash

# avi to mp4
ffmpeg -i SuiB_2018_timelapse.avi -c:v libx264 -crf 19 -preset slow -c:a libfdk_aac -b:a 192k -ac 2 SuiB_2018_timelapse.mp4