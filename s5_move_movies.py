#!/usr/bin/env python

import shutil
import os.path as osp
import misc
    
year = 2019 

workspace = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\{}'.format(year)
  
pic_diff = misc.read_pickle(osp.join(workspace, 'switch.pickle'))

if pic_diff > 0: 

    shutil.copyfile(r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\SuiB_2019_timelapse.gif', 
                    r'C:\Users\Ch.Kienholz\Google Drive\SuiB\SuiB_2019_timelapse.gif')
    
    shutil.copyfile(r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\SuiB_2019_timelapse_vector.gif', 
                    r'C:\Users\Ch.Kienholz\Google Drive\SuiB\SuiB_2019_timelapse_vector.gif')