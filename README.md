# timelapse_tracking

### About

A suite of Python and Unix shell scripts to download and annotate timelapse images taken at Mendenhall Glacier's Suicide Basin. Code to track features between subsequent timelapse images is also provided. 

Some of the results are posted on https://www.weather.gov/ajk/suicideBasin

Written and tested in Python 2.7.