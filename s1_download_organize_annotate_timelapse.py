# -*- coding: utf-8 -*-
"""
Created on Tue Jun 05 2018

@author: Ch.Kienholz
"""
     
import imaplib
import email
import datetime as dt
import glob
import os, shutil
import os.path as osp

from PIL import Image
import numpy as np
import pandas as pd

import matplotlib as mpl 
import matplotlib.pyplot as plt
mpl.rc("font", **{"sans-serif": ["Arial"]})

import misc

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

try:
    import credentials
except:
    raise Exception('send email to christian.kienholz@gmail.com to request credentials')
                               
class PhotoDownloader(object):
    '''
    Class to download photo attachments sent from nupoint server to gmail address
    '''
    
    def __init__(self, user=None, password=None, outputdir=None, date=None):
                                      
        self.server = 'imap.gmail.com'
        self.user = user
        self.password = password
        self.outputdir = outputdir
        self.address = '13000107@nupointnetworks.com'
        self.date = date
        
    def connect(self):
        '''
        connect to an IMAP server
        '''
        m = imaplib.IMAP4_SSL(self.server)
        m.login(self.user, self.password)
        m.select()
        return m
    
    def download_attachments_email(self, m, emailid):
        '''
        download all attachment files for a given email
        '''
        resp, data = m.fetch(emailid, "(BODY.PEEK[])")
        email_body = data[0][1]
        mail = email.message_from_string(email_body)
        if mail.get_content_maintype() != 'multipart':
            pass
        for part in mail.walk():
            if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
                open(self.outputdir + '/' + part.get_filename(), 'wb').write(part.get_payload(decode=True))
    
    def download(self):
        '''
        download all the attachment files for all emails in the inbox
        '''
        m = self.connect()
       
        criterion = '(FROM {} SINCE {})'.format(self.address, self.date)
        resp, items = m.search(None, criterion)     
            
        items = items[0].split()
        for emailid in items:
            self.download_attachments_email(m, emailid)
            
        m.logout()
            
#%%

def check_image_number(directory):
    pics = glob.glob(osp.join(directory, '*.jpg'))
    return len(pics)
    
def del_night_images(directory):
    
    timestamps = ['{:02d}'.format(num) for num in range(7, 12)]
    
    for timestamp in timestamps: 

        # delete pictures taken between (7:00 and 12:00 am UTC).
        pictures = glob.glob(directory + '/*{}????.jpg'.format(timestamp))
        
        for picture in pictures:
            os.remove(picture)
            
def keep_sixam_images(directory):
    
    timestamps = ['{:02d}'.format(num) for num in range(1, 25)]
    timestamps.remove('14')
      
    for timestamp in timestamps: 

        # delete all pictures except (14:00 UTC).
        pictures = glob.glob(directory + '/*{}????.jpg'.format(timestamp))
        
        for picture in pictures:
            os.remove(picture)
            
def move_images(directory_in, directory_out, number=1):
    '''
    copy all or a certain number of images from one folder to another
    '''

    # move the latest photos
    photos = sorted(glob.glob(osp.join(directory_in, '*.jpg')))[-1 * number:]
    
    for photo in photos:
    
        shutil.copy(photo, directory_out)
        
#%%
                           
def annotate_images(workspace, logo_ws, pic_diff, annotate_all=0):
    '''
    annotates time-lapse photos with timestamps, vertical scale, and agency logos
    '''
    
    if annotate_all == 1: 
        images = sorted(glob.glob('{}\*.jpg'.format(workspace)))
    else:
        images = sorted(glob.glob('{}\*.jpg'.format(workspace)))[-pic_diff:]
        
    plt.ioff()
    
    for image in images:
                
        # get time into AK daylight savings time
        try:
            time_object = misc.hour_rounder(dt.datetime.strptime(osp.basename(image), 
                                            'Suicide_%Y%m%d%H%M%S.jpg')) - dt.timedelta(hours=8)
        except:
            time_object = misc.hour_rounder(dt.datetime.strptime(osp.basename(image), 
                                            'SuicideBasin_%Y%m%d%H%M%S.jpg')) - dt.timedelta(hours=8)

        datestring = time_object.strftime('%Y-%m-%d')
        timestring = time_object.strftime('%I:%M %p')
        
        fig, ax = plt.subplots(1, 1, figsize=(12, 9), facecolor='w')
                
        frame = np.array(Image.open(image))
        ax.imshow(frame, interpolation=None)
        
        #---------------------------------------
        # vertical scale         
        
        for meter in range(370, 481, 10):
            
            [x_pixel, y_pixel] = misc.pixel_elev_from_meters(time_object.year, meter)
            
            ax.plot([x_pixel - 2, x_pixel + 2], [y_pixel, y_pixel], '-', color='red', lw=1.2)
            
            ax.text(x_pixel + 19, y_pixel, str(meter) + ' m', color='white', fontsize=8, 
                    verticalalignment='center', zorder=100)
        
        misc.annotatefun(ax, [datestring, timestring], 0.99, 0.17, 
                        ydiff=0.05, fonts=20, col='w', ha='right')
         
        ax.axis('off')                  

        # zoomed inset
        if time_object.year == 2019:

            axins = zoomed_inset_axes(ax, 2.5, loc=1)
            axins.spines['bottom'].set_color('darkgray')
            axins.spines['top'].set_color('darkgray') 
            axins.spines['right'].set_color('darkgray')
            axins.spines['left'].set_color('darkgray')
            
            axins.imshow(frame, interpolation=None)
                    
            # sub region of the original image
            x1, x2, y1, y2 = 160, 250, 570, 410
            axins.set_xlim(x1, x2)
            axins.set_ylim(y1, y2)
            
            axins.set_xticklabels([])
            axins.set_yticklabels([])
            axins.set_xticks([])
            axins.set_yticks([])
            
            # draw a bbox of the region of the inset axes in the parent axes
            [pp, p1, p2] = mark_inset(ax, axins, loc1=3, loc2=3, fc='none', 
                                      edgecolor='darkgray')
            
            # set connector lines to invisible
            p1.set_visible(0)
            p2.set_visible(0)
        
            for meter in range(380, 441, 1):  
    
                [x_pixel, y_pixel] = misc.pixel_elev_from_meters(time_object.year, meter)
                
                if meter in range(380, 441, 10):
                    axins.plot([x_pixel - 2, x_pixel + 2], [y_pixel, y_pixel], 
                               '-', color='red', lw=1)
                    axins.text(x_pixel + 19, y_pixel, str(meter) + ' m', 
                               color='white', fontsize=10, 
                               verticalalignment='center', zorder=100)
                               
                elif meter in range(380, 441, 5):                    
                    axins.plot([x_pixel - 1.5, x_pixel + 1.5], [y_pixel, y_pixel], 
                               '-', color='red', lw=1)
                       
                else:
                    axins.plot([x_pixel - 1, x_pixel + 1], [y_pixel, y_pixel], 
                               '-', color='red', lw=1)
                    
        #---------------------------------------
        # agency logos                   
        
        axlocations = [[0.047, 0.11, 0.11, 0.11], 
                       [0.02, 0.023, 0.14, 0.14],
                       [0.84, 0.023, 0.14, 0.14],
                       [0.74, 0.023, 0.1, 0.1]]
                       
        logos = ['UAS_logo_white.png', 'AKCASC_white.png', 
                 'USGS_white.png', 'nws_logo.png']
                
        for axlocation, logo in zip(axlocations, logos):
            
            axins = fig.add_axes(axlocation, anchor='SW', zorder=100)
                
            axins.patch.set_visible(False)
            
            image = osp.join(logo_ws, logo)
                                      
            frame = np.array(Image.open(image))
            axins.imshow(frame)            
                            
            axins.set_xlim([0, frame.shape[1]])
            axins.set_ylim([frame.shape[0], 0])
            axins.axis('off')
            
        #--------------------------------------- 
        
        fig.tight_layout()
        
        fig.subplots_adjust(left=0.01, bottom=0.01, top=0.99, right=0.99)
                        
        plt.savefig('{}/{}.png'.format(workspace, time_object.strftime('%Y%m%d%H%M')), 
                    format='png', dpi=100)
        
        # save with an early date '20190101'
        # this ensures that the first and last image in the resulting .gif
        # is the first one              
        plt.savefig('{}/{}.png'.format(workspace, '201901010000'), format='png', dpi=100)
                    
        if time_object.year >= 2019:
            # save one level higher than the workspace
            fig.savefig(osp.dirname(osp.dirname(workspace)) + '/Latest_image_scale.png', dpi=200)
            
            # save on google drive to share
            fig.savefig(r'C:\Users\Ch.Kienholz\Google Drive\SuiB' + '/Latest_image_scale.png', dpi=200)
            
        plt.close(fig) 
        
#%%

def compare_2018_2019_images():
    '''
    plots the latest 2019 image (no real-time gauge) and several 2018 images 
    that have similar water levels
    on the 2018 images, the water level is annotated, so the 2019 water level 
    can be constrained by visually comparing the 2018 and 2019 images    
    '''
    
    workspace = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera'
    workspace_gauge = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\data'
    
    year = 2019
    
    # get the latest image from 2019
    image_2019 = glob.glob(osp.join(workspace, '{}/Suicide_{}*.jpg'.format(year, year)))[-1]
    
    # determine appropriate 2018 target date where the 2018 image shows approx.
    # the same water level as the 2019 image
    # adapt manually if needed
    target_date_2018 = dt.datetime.strftime(pd.to_datetime('20180619') 
                     + (dt.datetime.now() - pd.to_datetime('20190614')), '%Y%m%d')
    
    date18 = dt.datetime.strptime(osp.basename(target_date_2018), '%Y%m%d')
    
    # select several images around the 2018 date
    # the range variable determines how many images are selected
    dates = [dt.datetime.strftime(date18 + dt.timedelta(days=day), '%Y%m%d') for day in range(-3, 4)]
        
    images_2018 = []
    
    for date in dates:
        try:
            images_2018.append(glob.glob(osp.join(workspace, '2018\Suicide_{}*.jpg'.format(date)))[0])
        except:
            pass
            
    # shifts to adapt the axes so that 2018 and 2019 images show approximately the same extent
    # adapt manually if needed 
    x_shift = -93.6597
    y_shift = -71.9633
    
    xlim_array = np.array([630, 830])
    ylim_array = np.array([520, 280])
        
    for c, image_2018 in enumerate(images_2018):
    
        # plot the image
        im_a = np.array(Image.open(image_2019))
        im_b = np.array(Image.open(image_2018))
        
        fig = plt.figure(figsize=(6.8, 8.0), facecolor='w')
               
        ax1 = fig.add_subplot(1, 1, 1)  
                
        ax1.imshow(im_a, interpolation=None)
        
        ax1.set_xticks(np.arange(0, 1000, 20))
        ax1.set_yticks(np.arange(0, 1000, 20))
        ax1.set_xticklabels([])
        ax1.set_yticklabels([])
                
#        ax1.plot([xline_cord, xline_cord], [0, 1000], '-', color='white')
#        ax1.plot([0, 1000], [yline_cord, yline_cord], '-', color='white')
        
        ax1.set(xlim=xlim_array, ylim=ylim_array)
        ax1.grid(color='white', linestyle='dotted', alpha=0.5)

        date1 = dt.datetime.strptime(osp.basename(image_2019), 
                'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        date1_str = dt.datetime.strftime(date1, '%Y-%m-%d %I:%M %p')        
        misc.annotatefun(ax1, [date1_str], 0.5, 0.95, col='white', fonts=15)
        
        fig.tight_layout()
        fig.subplots_adjust(right=1.05, left=-0.05, top=0.985, bottom=0.02)
        
        fig.savefig(osp.join(workspace, 'comparison_{}a.png'.format(c)))
        plt.close(fig)
        
        #----------------------------------------------------------------------

        fig = plt.figure(figsize=(6.8, 8.0), facecolor='w')        
        
        ax2 = fig.add_subplot(1, 1, 1)  
                
        ax2.imshow(im_b, interpolation=None)
        
        # apply the shifts so the figures show approximately the same extent
        ax2.set_xticks(np.arange(0, 1000, 20) - x_shift)
        ax2.set_yticks(np.arange(0, 1000, 20) - y_shift)
        ax2.set_xticklabels([])
        ax2.set_yticklabels([])
                
        ax2.set(xlim=xlim_array - x_shift, ylim=ylim_array - y_shift) 
        
        ax2.grid(color='white', linestyle='dotted', alpha=0.5)
    
#        ax2.plot(np.array([xline_cord, xline_cord]) - x_shift, np.array([0, 1000]) - y_shift, '-', color='white')
#        ax2.plot(np.array([0, 1000]) - x_shift, np.array([yline_cord, yline_cord]) - y_shift, '-', color='white')    
    
        date2 = dt.datetime.strptime(osp.basename(image_2018), 
                'Suicide_%Y%m%d%H%M%S.jpg') - dt.timedelta(hours=8)
        
        date2_str = dt.datetime.strftime(date2, '%Y-%m-%d %I:%M %p')

        # try-except since the water was below the gauge early in the 2018 record
        try:
            waterlevel_2018 = pd.read_pickle(osp.join(workspace_gauge, 
                                            'SuiB_water_level_2018.pickle'))
            wl = waterlevel_2018[misc.hour_rounder(date2)]
            
            misc.annotatefun(ax2, [date2_str, str(round(wl, 1)) + ' m a.s.l.'], 
                                   0.5, 0.95, col='white', fonts=15)
        except:
            print 'found no water level measurement for: {}'.format(misc.hour_rounder(date2))
            misc.annotatefun(ax2, [date2_str], 0.5, 0.95, col='white', fonts=15)
            
        fig.tight_layout()
        fig.subplots_adjust(right=1.05, left=-0.05, top=0.985, bottom=0.02)
        
        fig.savefig(osp.join(workspace, 'comparison_{}b.png'.format(c)))
        plt.close(fig)
        
#%%

def main():

    # current year
    year = 2019
    
    # directory that stores all the images
    dir_B = r'B:\4_Mendenhall\Photographs\Timelapse\{}'.format(year)
    
    # working directory (some of the photos get moved around and deleted)
    dir_C = r'C:\Users\Ch.Kienholz\Dropbox\Public\SuiB\camera\{}'.format(year)
    dir_logos = r'C:\Users\Ch.Kienholz\Dropbox\Mendenhall\Scripts\timelapse_tracking\logos'
    
    # count the number of photos in the target directory before download
    pics_before = check_image_number(directory=dir_B)
    
    # retrieve user and password for email account
    user = credentials.user()
    password = credentials.password()
    
    # download same day's photos from email account 
    date = (dt.date.today()).strftime('%d-%b-%Y') # - dt.timedelta(3)
    PhotoDownloader(user=user, password=password, 
                    outputdir=dir_B, date=date).download() 
    
    # recount the number of photos in the target directory
    pics_after = check_image_number(directory=dir_B) 
    
    # check whether we've downloaded any images 
    pic_diff = pics_after - pics_before
    
    # write the number of downloaded photos into pickle file
    # pickle file is used in following scripts
    misc.write_pickle(osp.join(dir_C, 'switch.pickle'), pic_diff)
               
    if pic_diff > 0: 
        
        # download images to the working directory
        PhotoDownloader(user=user, password=password, 
                        outputdir=dir_C, date=date).download() 
        
        # back up images to another folder        
        move_images(directory_in=dir_C, 
                    directory_out=osp.join(dir_C, 'movie_high_res'), 
                    number=pic_diff)
        
        # delete dark images
        del_night_images(directory=dir_C)
    
        # copy images to another folder 
        move_images(directory_in=dir_C, directory_out=osp.join(dir_C, 'movie'), 
                    number=pic_diff)
        
        # keep only 6:00 am images for feature tracking (no sun shadows)
        keep_sixam_images(directory=dir_C)
        
        # copy the images yet to another folder (used for tracking with different settings) 
        move_images(directory_in=dir_C, directory_out=osp.join(dir_C, 'pixel_tracking_plot'), 
                    number=pic_diff)
        
        #-----------------------------------------
        # call function that annotates time-lapse photos with timestamps, 
        # vertical scale, and agency logos
        annotate_images(osp.join(dir_C, 'movie'), dir_logos, pic_diff, annotate_all=0)        
        annotate_images(osp.join(dir_C, 'movie_high_res'), dir_logos, pic_diff, annotate_all=0) 
        
        # call function that compares the 2018 to the 2019 photos
        compare_2018_2019_images()
                   
if __name__ == '__main__':
    
    main()