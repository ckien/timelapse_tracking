#!/bin/bash 

# -x

# This creates a .gif that works in media player

workspace=$1
delay=$2
searchpattern=$3
videoname=$4

cd $workspace

convert -delay $delay -loop 0 $searchpattern $videoname.gif